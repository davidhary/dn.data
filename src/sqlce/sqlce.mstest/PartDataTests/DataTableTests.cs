using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.SqlCe.MSTest
{

    /// <summary> SQL CE Data tables unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class DataTableTests
    {

        #region " PART DATA "

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> (Unit Test Method) tests read part table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [DataSource( "System.Data.SqlServerCe.4.0", @"Data Source=C:\my\lib\vs\data\data\src\sqlce\sqlce.mstest\bin\Debug\net48\PartDataTests\TestData.sdf", "Part", DataAccessMethod.Sequential )]
        [TestMethod()]
        public void PartTableShouldRead()
        {
            this.TestContext.DataConnection.ConnectionString = @"Data Source=C:\my\lib\vs\data\data\src\sqlce\sqlce.mstest\bin\Debug\net48\PartDataTests\TestData.sdf";
            var part = new PartInfo( this.TestContext );
            System.Console.Out.WriteLine( $"Got part number {part.PartNumber}" );
            double expectedVoltage = 5d;
            double actualVoltage = part.NominalVoltage;
            Assert.AreEqual( expectedVoltage, actualVoltage, $"{part.PartNumber}" );
            var existing = new int[] { 201703161, 114836, 77072 };
            if ( existing.Contains( part.PartNumber ) )
                Assert.AreEqual( part.PartNumber, part.NominalInfo.PartNumber );
        }

        #endregion

    }
}
