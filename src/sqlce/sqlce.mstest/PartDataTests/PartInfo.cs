using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.SqlCe.MSTest
{

    /// <summary> Information about the part. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    public class PartInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="testContext"> Context for the test. </param>
        public PartInfo( TestContext testContext ) : this( TestContextDataAccess.ValidatedDataRow( testContext ) )
        {
            this.PopulateRelatedRecords( TestContextDataAccess.ValidatedConnection( testContext ) );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="row"> The row. </param>
        public PartInfo(DataRow row) : base()
        {
            if (row is null)
            {
                throw new ArgumentNullException(nameof(row));
            }
            else
            {
                this.PartNumber = row.Field<int>( nameof( isr.Data.SqlCe.MSTest.PartInfo.PartNumber ) );
                this.NominalVoltage = row.Field< double>(nameof( isr.Data.SqlCe.MSTest.PartInfo.NominalVoltage ));
                this.NominalCurrent = row.Field<double>( nameof( isr.Data.SqlCe.MSTest.PartInfo.NominalCurrent));
                this.NominalOutput = row.Field<double>( nameof( isr.Data.SqlCe.MSTest.PartInfo.NominalOutput));
            }
        }

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public int PartNumber { get; set; }

        /// <summary> Gets or sets the nominal voltage. </summary>
        /// <value> The nominal voltage. </value>
        public double NominalVoltage { get; set; }

        /// <summary> Gets or sets the nominal current. </summary>
        /// <value> The nominal current. </value>
        public double NominalCurrent { get; set; }

        /// <summary> Gets or sets the nominal output. </summary>
        /// <value> The nominal output. </value>
        public double NominalOutput { get; set; }

        /// <summary> Gets or sets information describing the nominal. </summary>
        /// <value> Information describing the nominal. </value>
        public NominalInfo NominalInfo { get; set; }

        /// <summary> Gets or sets the name of the nominal table. </summary>
        /// <value> The name of the nominal table. </value>
        public static string NominalTableName { get; set; } = "Nominal";

        /// <summary> Gets or sets the part number record query format. </summary>
        /// <value> The part number record query format. </value>
        public static string PartNumberRecordQueryFormat { get; set; } = "SELECT * FROM {0} WHERE PartNumber={1}";

        /// <summary>   Populates a related records. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connectionString"> The connection string. </param>
        public void PopulateRelatedRecords(string connectionString )
        {
            if (string.IsNullOrWhiteSpace( connectionString ))
                throw new ArgumentNullException(nameof( connectionString ) );
            var connection = isr.Data.Sqlce.DatabaseManager.OpenDbConnection( connectionString );
            string query = string.Format(PartNumberRecordQueryFormat, NominalTableName, this.PartNumber );
            using var dt = Sqlce.DatabaseManager.FillTable( connection, NominalTableName, query );
            this.NominalInfo = (dt.Rows?.Count) > 0 == true ? new NominalInfo( dt.Rows[0] ) : null;
        }

        /// <summary> Populates a related records. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        public void PopulateRelatedRecords1(IDbConnection connection)
        {
            if (connection is null)
                throw new ArgumentNullException(nameof(connection));
            // Dim conn As New System.Data.SqlServerCe.SqlCeConnection With {.ConnectionString = "Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf"}
            // THe test context gets disposed killing the connection. 
            // var conn = new System.Data.SqlServerCe.SqlCeConnection() { ConnectionString = connection.ConnectionString };
            using var dt = new DataTable( NominalTableName ) {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            string query = string.Format( PartNumberRecordQueryFormat, NominalTableName, this.PartNumber );
            // Using dad As New SqlServerCe.SqlCeDataAdapter(query, TryCast(connection, SqlServerCe.SqlCeConnection))
            using var dad = new System.Data.SqlServerCe.SqlCeDataAdapter( query, ( System.Data.SqlServerCe.SqlCeConnection ) connection );
            _ = dad.Fill( dt );
            this.NominalInfo = (dt.Rows?.Count) > 0 == true ? new NominalInfo( dt.Rows[0] ) : null;
        }

        /// <summary>   Populates a related records. </summary>
        /// <remarks>   David, 2022-02-08. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="connection">   The connection. </param>
        public void PopulateRelatedRecords( IDbConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            // Dim conn As New System.Data.SqlServerCe.SqlCeConnection With {.ConnectionString = "Data Source=C:\My\Libraries\VS\Data\Data\Tests\Sqlce\bin\Debug\PartDataTests\TestData.sdf"}
            // THe test context gets disposed killing the connection. 
            var conn = new System.Data.SqlServerCe.SqlCeConnection() { ConnectionString = connection.ConnectionString };
            using var dt = new DataTable( NominalTableName ) {
                Locale = System.Globalization.CultureInfo.CurrentCulture
            };
            string query = string.Format( PartNumberRecordQueryFormat, NominalTableName, this.PartNumber );
            // Using dad As New SqlServerCe.SqlCeDataAdapter(query, TryCast(connection, SqlServerCe.SqlCeConnection))
            using var dad = new System.Data.SqlServerCe.SqlCeDataAdapter( query, conn );
            _ = dad.Fill( dt );
            this.NominalInfo = (dt.Rows?.Count) > 0 == true ? new NominalInfo( dt.Rows[0] ) : null;
        }


    }

    /// <summary> Information about the nominal. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-13 </para>
    /// </remarks>
    public class NominalInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="row"> The row. </param>
        public NominalInfo(DataRow row) : base()
        {
            this.ParseDataRow(row);
        }

        /// <summary> Parse data row. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="row"> The row. </param>
        private void ParseDataRow(DataRow row)
        {
            if (row is null)
                throw new ArgumentNullException(nameof(row));
            this.PartNumber = row.Field<int>( nameof( isr.Data.SqlCe.MSTest.NominalInfo.PartNumber ) );
            this.NominalValue = row.Field<double>( nameof( isr.Data.SqlCe.MSTest.NominalInfo.NominalValue ) );
            this.Tolerance = row.Field<double>( nameof( isr.Data.SqlCe.MSTest.NominalInfo.Tolerance ) );
        }

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public int PartNumber { get; set; }

        /// <summary> Gets or sets the nominal value. </summary>
        /// <value> The nominal value. </value>
        public double NominalValue { get; set; }

        /// <summary> Gets or sets the tolerance. </summary>
        /// <value> The tolerance. </value>
        public double Tolerance { get; set; }
    }
}
