using System;

namespace isr.Data.Sqlce.ExceptionExtensions
{

    /// <summary>
    /// Exception methods for adding exception data and building a detailed exception message.
    /// </summary>
        /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary>
        /// Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns>
        /// <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        /// </returns>
        public static bool AddExceptionData(Exception value, System.Data.SqlServerCe.SqlCeException exception)
        {
            if (value is object && exception is object)
            {
                value.Data.Add($"{value.Data.Count}-ErrorCode", exception.ErrorCode);
                if ((exception.Errors?.Count) > 0 == true)
                {
                    foreach (System.Data.SqlServerCe.SqlCeError err in exception.Errors)
                        value.Data.Add($"{value.Data.Count}-Error{err.NativeError}", err.ToString());
                }
            }

            return exception is object;
        }

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        public static bool AddExceptionData(this Exception exception)
        {
            return AddExceptionData(exception, exception as System.Data.SqlServerCe.SqlCeException) ||
                   isr.Data.Core.ExceptionExtensions.ExceptionDataMethods.AddExceptionData( exception );
        }

    }
}
