using System;
using System.Data;
using System.IO;
using System.Reflection;

using isr.Data.Sqlce.ExceptionExtensions;

namespace isr.Data.Sqlce
{

    /// <summary> Implements a database manager entity for SQL Compact Edition. </summary>
    /// <remarks>
    /// David, 2009-10-21, 1.2.3581 <para>
    /// David, 2011-09-16, 1.2.4276. Use namespace isr.Data.Sqlce. </para><para>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class DatabaseManager : Core.DatabaseManagerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="Core.DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        public DatabaseManager(string connectionString) : base(connectionString)
        {
            this.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Core.DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <param name="databaseName">     Name of the database. </param>
        public DatabaseManager(string connectionString, string databaseName) : base(connectionString, databaseName)
        {
            this.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)";
        }

        #endregion

        #region " PRESETTABLE "

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public override void InitializeKnownState()
        {
        }

        #endregion

        #region " ASSEMBLY INFO "

        /// <summary>   Gets an assembly attribute. </summary>
        /// <remarks>   David, 2021-02-16. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="assembly"> The assembly. </param>
        /// <returns>   The assembly attribute. </returns>
        public static T GetAssemblyAttribute<T>( Assembly assembly ) where T : Attribute
        {
            // Get attributes of this type.
            object[] attributes = assembly.GetCustomAttributes( typeof( T ), true );

            // If we didn't get anything, return null.
            if ( (attributes == null) || (attributes.Length == 0) ) return null;

            // Convert the first attribute value into the desired type and return it.
            return ( T ) attributes[0];
        }

        /// <summary>   Gets company name. </summary>
        /// <remarks>   David, 2022-02-07. </remarks>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns>   The company name. </returns>
        public static string GetCompanyName( Assembly assembly,  string defaultValue = "" )
        {
            AssemblyCompanyAttribute companyAttr =  GetAssemblyAttribute<AssemblyCompanyAttribute>( assembly );
            return ( companyAttr is object ) ? companyAttr.Company : defaultValue;

        }

        /// <summary>   Gets product name. </summary>
        /// <remarks>   David, 2022-02-07. </remarks>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns>   The product name. </returns>
        public static string GetProductName( Assembly assembly, string defaultValue = "" )
        {
            AssemblyProductAttribute productAttr =
                GetAssemblyAttribute<AssemblyProductAttribute>( assembly );
            return (productAttr is object) ? productAttr.Product : defaultValue;

        }

        /// <summary>   Gets file version. </summary>
        /// <remarks>   David, 2022-02-07. </remarks>
        /// <param name="assembly">     The assembly. </param>
        /// <param name="defaultValue"> (Optional) The default value. </param>
        /// <returns>   The file version. </returns>
        public static Version GetFileVersion( Assembly assembly, string defaultValue = "1.0.0.0" )
        {
            AssemblyFileVersionAttribute fileVersionAttr = GetAssemblyAttribute<AssemblyFileVersionAttribute>( assembly );
            return ( fileVersionAttr is object ) ? new Version( fileVersionAttr.Version ) : new Version( defaultValue );

        }

        #endregion

        #region " CONNECTION INFO "

        #region " BUILDERS "

        /// <summary>
        /// Returns the application data folder using the application company and product name or
        /// directory path. In design mode the data folder is the program folder such as c:\Program File\
        /// ISR\ISR Assembly 7.0\Program In production, this is the application data folder product name
        /// such as Application Data\ISR\ISR Assembly 7.0\major.minor.revision under Windows XP or
        /// ProgramData\ISR\ISR Assembly 7.0\major.minor.revision under Vista.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="useApplicationFolder"> true to use application folder. </param>
        /// <param name="allUsers">             true to all users. </param>
        /// <returns> Application data folder. </returns>
        public static string BuildApplicationDataFolderName(bool useApplicationFolder, bool allUsers)
        {
            string path;
            var assembly = System.Reflection.Assembly.GetEntryAssembly();
            if (useApplicationFolder)
            {
                var fi = new System.IO.FileInfo( assembly.Location );
                path = fi.DirectoryName;
            }
            else
            {
                string specialPath = allUsers
                                        ? Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData )
                                        : Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData );

                var dir = new System.IO.DirectoryInfo( specialPath );
                // get the top folder so that we can create our own path invariant of the executing assembly.
                specialPath = dir.Parent.Parent.FullName;

                // use the company name and product name to the actual path.
                path = System.IO.Path.Combine(specialPath, DatabaseManager.GetCompanyName( assembly, "ISR" ));
                path = System.IO.Path.Combine(path, DatabaseManager.GetCompanyName( assembly, nameof(isr.Data.Sqlce) ) );
                path = System.IO.Path.Combine(path, DatabaseManager.GetFileVersion( assembly ).ToString(3));
            }

            // return the full path for the folder.
            return path;
        }

        /// <summary>
        /// Returns a standard data folder name.
        /// In design mode the data folder is a sibling to the program folder. In production, the data
        /// folder resides under the application data folder product name.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="useApplicationFolder"> true to use application folder. </param>
        /// <param name="allUsers">             true to all users. </param>
        /// <param name="folderTitle">          . </param>
        /// <param name="useSiblingFolder">     true to use sibling folder. </param>
        /// <returns> Application data folder. </returns>
        public static string BuildApplicationDataFolderName(bool useApplicationFolder, bool allUsers, string folderTitle, bool useSiblingFolder)
        {
            string dataPath = BuildApplicationDataFolderName(useApplicationFolder, allUsers);
            if (useSiblingFolder)
            {
                dataPath = Directory.GetParent( dataPath ).FullName;
            }

            return System.IO.Path.Combine(dataPath, folderTitle);
        }

        /// <summary> Builds a default SQL Compact Edition connection string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="databaseName">          Name of the database. </param>
        /// <param name="databaseFolderTitle">   The database folder title. </param>
        /// <param name="databaseFileExtension"> The database file extension. </param>
        /// <returns> Connection string for SQL Compact Edition. </returns>
        public static string BuildConnectionString(string databaseName, string databaseFolderTitle, string databaseFileExtension)
        {
            string folderPath = BuildApplicationDataFolderName(true, true, databaseFolderTitle, true);
            string filePath = System.IO.Path.Combine(folderPath, databaseName + databaseFileExtension);
            return $"Data Source={filePath}";
        }

        #endregion

        #region " PARSERS "

        /// <summary> Parses the compact connection string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="connectionString"> The connection string. </param>
        public override void ParseConnectionString(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            var elements = connectionString.Split('=');
            if (elements.Length < 2)
            {
                throw new ArgumentException($"Invalid connection string '{connectionString}'.", nameof(connectionString));
            }

            this.ServerName = "SQLCE";
            this.InstanceName = "SQLCE";
            this.DatabaseFileInfo = new System.IO.FileInfo(elements[1].Trim().Trim(';'));
            this.DatabaseName = this.DatabaseFileInfo.Name.Substring(0, this.DatabaseFileInfo.Name.LastIndexOf( this.DatabaseFileInfo.Extension, StringComparison.OrdinalIgnoreCase));
            this.DataSourceCompatibilityLevel = 4;
        }

        #endregion

        #endregion

        #region " CONNECTION MANAGER "

        /// <summary>   Pings the database server. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <param name="pingHops"> (Optional) The ping hops. </param>
        /// <returns>   True if the server was located; otherwise, false. </returns>
        public override bool PingHost(TimeSpan timeout, int pingHops = 3)
        {
            return isr.Data.Core.DatabaseManagerBase.FastPing( this.ServerName, pingHops, timeout );
        }

        /// <summary> Creates the connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The new connection. </returns>
        public static System.Data.SqlServerCe.SqlCeConnection CreateConnection()
        {
            System.Data.SqlServerCe.SqlCeConnection conn = null;
            System.Data.SqlServerCe.SqlCeConnection temp = null;
            try
            {
                temp = new System.Data.SqlServerCe.SqlCeConnection();
                conn = temp;
                temp = null;
            }
            catch
            {
                throw;
            }
            finally
            {
                temp?.Dispose();
            }

            return conn;
        }

        /// <summary>   Opens database connection. </summary>
        /// <remarks>   David, 2022-02-07. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns>   A System.Data.IDbConnection. </returns>
        public static System.Data.IDbConnection OpenDbConnection( string connectionString )
        {
            return DatabaseManager.OpenConnection( connectionString );

        }
        /// <summary> Opens a connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns> A SqlServerCe.SqlCeConnection. </returns>
        public static System.Data.SqlServerCe.SqlCeConnection OpenConnection(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            System.Data.SqlServerCe.SqlCeConnection conn = null;
            System.Data.SqlServerCe.SqlCeConnection temp = null;
            try
            {
                temp = CreateConnection();
                temp.ConnectionString = connectionString;
                temp.Open();
                conn = temp;
                temp = null;
            }
            catch
            {
                throw;
            }
            finally
            {
                temp?.Dispose();
            }

            return conn;
        }

        /// <summary>
        /// Returns true if able to connect to the database using the specified connection string.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectionString">   Specifies the connection string to connect to. </param>
        /// <param name="keepConnectionOpen"> True to keep connection open. </param>
        /// <returns> True if connected. </returns>
        public override (bool Success, string Details) TryConnect(string connectionString, bool keepConnectionOpen)
        {
            (bool Success, string Details) result = (true, string.Empty);
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            string activity = string.Empty;
            System.Data.SqlServerCe.SqlCeConnection connection = null;
            try
            {
                // connect to the local database.
                activity = $"creating connection using {connectionString}";
                connection = new System.Data.SqlServerCe.SqlCeConnection(connectionString);
                activity = $"opening connection {connectionString}";
                connection.Open();
                this.LastConnectionFailed = IsClosed(connection);
            }
            catch (Exception ex)
            {
                this.LastConnectionFailed = true;
                _ = ex.AddExceptionData();
                result = (false, $"Exception {activity};. {ex}");
            }
            finally
            {
                if (connection is object)
                {
                    if (!keepConnectionOpen)
                    {
                        // apparently, close also disposes the connection. Otherwise, uncomment this:
                        // If not DatabaseManager.IsClosed(connection)  Then connection.Close()
                        connection.Dispose();
                    }
                }
            }

            return result;
        }

        #endregion

        #region " COMMAND "

        /// <summary> Executes the specified command and returns the number of rows affected. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="commandText">      Specifies a command. </param>
        /// <returns> Row count. </returns>
        public override int ExecuteCommand(string connectionString, string commandText)
        {
            int rowCount = 0;
            using (var connection = new System.Data.SqlServerCe.SqlCeConnection(connectionString))
            {
                var sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = commandText;
                connection.Open();
                rowCount = sqlCommand.ExecuteNonQuery();
                // note the close also disposes. Otherwise, do: connection.Close()
            }

            return rowCount;
        }

        #endregion

        #region " DATA TABLE "

        /// <summary> Opens a table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <param name="tableName">  Name of the table. </param>
        /// <returns> A DataTable. </returns>
        public static DataTable FillTable(System.Data.SqlServerCe.SqlCeConnection connection, string tableName)
        {
            return connection is null
                ? throw new ArgumentNullException(nameof(connection))
                : string.IsNullOrWhiteSpace(tableName)
                    ? throw new ArgumentNullException(nameof(tableName))
                    : FillTable(connection, tableName, $"SELECT * FROM {tableName}");
        }

        /// <summary> Opens a table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connection">   The connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> A DataTable. </returns>
        public static DataTable FillTable(IDbConnection connection, string tableName, string queryCommand)
        {
            return FillTable(connection as System.Data.SqlServerCe.SqlCeConnection, tableName, queryCommand);
        }

        /// <summary> Opens a table. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="connection">   The connection. </param>
        /// <param name="tableName">    Name of the table. </param>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> A DataTable. </returns>
        public static DataTable FillTable(System.Data.SqlServerCe.SqlCeConnection connection, string tableName, string queryCommand)
        {
            if (connection is null)
                throw new ArgumentNullException(nameof(connection));
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentNullException(nameof(tableName));
            if (IsClosed(connection))
                connection.Open();
            if (IsClosed(connection))
            {
                throw new InvalidOperationException("Unable to open the connection to the database");
            }

            DataTable result = null;
            try
            {
                result = new DataTable(tableName) { Locale = System.Globalization.CultureInfo.CurrentCulture };
                using var dataAdapter = new System.Data.SqlServerCe.SqlCeDataAdapter( queryCommand, connection );
                _ = dataAdapter.Fill( result );
            }
            catch
            {
                if (result is object)
                    result.Dispose();
                throw;
            }

            return result;
        }

        #endregion

        #region " READER "

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="Core.ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <returns> <see cref="Core.ConnectedReader">connected reader</see>. </returns>
        public override Core.ConnectedReader ExecuteReader(string connectionString, string queryCommand)
        {
            return ExecuteReader(new System.Data.SqlServerCe.SqlCeConnection(connectionString), queryCommand);
        }

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="Core.ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">   Specifies a
        /// <see cref="System.Data.SqlServerCe.SqlCeConnection">connection</see>.
        /// </param>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> <see cref="Core.ConnectedReader">connected reader</see>. </returns>
        public static Core.ConnectedReader ExecuteReader(System.Data.SqlServerCe.SqlCeConnection connection, string queryCommand)
        {
            if (connection is null)
            {
                throw new ArgumentNullException(nameof(connection));
            }

            using var command = new System.Data.SqlServerCe.SqlCeCommand {
                Connection = connection,
                CommandType = CommandType.Text,
                CommandText = queryCommand
            };
            if ( IsClosed( connection ) )
                command.Connection.Open();
            System.Data.SqlServerCe.SqlCeDataReader reader = command.ExecuteResultSet( System.Data.SqlServerCe.ResultSetOptions.Scrollable );
            return new Core.ConnectedReader( connection, reader );
        }

        #endregion

        #region " UPDATE QUERY "

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public override int UpdateTableQuery(string connectionString, string query, bool ignoreMinorErrors)
        {
            return this.UpdateTableQuery(connectionString, PrepareCommandQuery(query), ignoreMinorErrors);
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public override int UpdateTableQuery(string connectionString, string[] query, bool ignoreMinorErrors)
        {
            using var connection = new System.Data.SqlServerCe.SqlCeConnection( connectionString );
            return this.UpdateTableQuery( connection, query, ignoreMinorErrors );
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">        Specifies a
        /// <see cref="System.Data.SqlServerCe.SqlCeConnection">connection</see>.
        /// </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public int UpdateTableQuery(System.Data.SqlServerCe.SqlCeConnection connection, string[] query, bool ignoreMinorErrors)
        {
            if (connection is null)
                throw new ArgumentNullException(nameof(connection));
            var recordCount = default(int);
            var isOpened = default(bool);
            this.QueryUpdateCount = 0;
            this.QueryErrorCount = 0;
            if (query is object)
            {
                using (var command = new System.Data.SqlServerCe.SqlCeCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    if (IsClosed(connection))
                    {
                        isOpened = true;
                        connection.Open();
                    }

                    foreach (string queryCommand in query)
                    {
                        if (!string.IsNullOrWhiteSpace(queryCommand))
                        {
                            // trim any leading or lagging control characters and spaces.
                            string cleanQueryCommand = queryCommand.Trim();
                            cleanQueryCommand = System.Text.RegularExpressions.Regex.Replace( cleanQueryCommand, @"\s+", " " );

                            if ( !string.IsNullOrWhiteSpace( cleanQueryCommand ) )
                            {
                                command.CommandText = cleanQueryCommand;
                                if (ignoreMinorErrors)
                                {
                                    try
                                    {
                                        // TO_DO: Check on this.
                                        // each update returns -1 if success or and error  if failed.
                                        recordCount += command.ExecuteNonQuery();
                                        this.QueryUpdateCount += 1;
                                    }
                                    catch (Exception)
                                    {
                                        this.QueryErrorCount += 1;
                                    }
                                    finally
                                    {
                                    }
                                }
                                else
                                {
                                    recordCount += command.ExecuteNonQuery();
                                    this.QueryUpdateCount += 1;
                                }
                            }
                        }
                    }
                }

                if (isOpened)
                {
                    connection.Close();
                }
            }

            return recordCount;
        }

        #endregion

    }
}
