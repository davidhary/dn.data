# About

isr.Data.Sqlce is a .Net library supporting SQL CE data operations.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Data.Sqlce is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Data Repository].

[Data Repository]: https://bitbucket.org/davidhary/dn.data

