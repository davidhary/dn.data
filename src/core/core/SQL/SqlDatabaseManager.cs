using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using isr.Data.Core.ExceptionExtensions;

namespace isr.Data.Core
{

    /// <summary> Implements a database manager entity for SQL Compact Edition. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-10-21, 1.2.3581.x. </para>
    /// </remarks>
    public class DatabaseManager : DatabaseManagerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        public DatabaseManager(string connectionString) : base(connectionString)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <param name="databaseName">     Name of the database. </param>
        public DatabaseManager(string connectionString, string databaseName) : base(connectionString, databaseName)
        {
        }

        #endregion

        #region " PRESET "

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public override void InitializeKnownState()
        {
            this.CurrentReleaseVersionQueryText = "SELECT ReleaseRevision, IsCurrentRelease FROM ReleaseHistory WHERE (IsCurrentRelease = 1)";
        }

        #endregion

        #region " CONNECTION INFO "

        #region " BUILDERS "

        /// <summary> A pattern specifying the connection string. </summary>
        /// <value> The connection string pattern. </value>
        protected static string ConnectionStringPattern { get; set; } = "data source={0};initial catalog={1};integrated security=SSPI;persist security info=False";

        /// <summary>
        /// Returns a connection string to an SQL catalog on the specified local server instance.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pattern">      Specifies the connection string pattern. </param>
        /// <param name="instanceName"> Specifies the local server instance name. </param>
        /// <param name="databaseName"> Specifies the database name. </param>
        /// <returns> A String. </returns>
        public static string BuildLocalConnectionString(string pattern, string instanceName, string databaseName)
        {
            if (string.IsNullOrWhiteSpace(pattern))
                throw new ArgumentNullException(nameof(pattern));
            if (string.IsNullOrWhiteSpace(databaseName))
                throw new ArgumentNullException(nameof(databaseName));
            if (instanceName is null)
                instanceName = string.Empty;
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, pattern, BuildLocalServerInstanceName(instanceName), databaseName);
        }

        /// <summary> Gets or sets the user connection string pattern. </summary>
        /// <value> The user connection string pattern. </value>
        protected static string UserConnectionStringPattern { get; set; } = "Data Source={0};Initial Catalog={1};User ID={2};Password={3}";

        /// <summary>
        /// Returns a connection string to an SQL catalog on the specified local server instance.
        /// </summary>
        /// <remarks> David, 2020-03-21. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pattern">      Specifies the connection string pattern. </param>
        /// <param name="instanceName"> Specifies the local server instance name. </param>
        /// <param name="databaseName"> Specifies the database name. </param>
        /// <param name="userName">     Name of the user. </param>
        /// <param name="password">     The password secure string. </param>
        /// <returns> A String. </returns>
        public static string BuildLocalConnectionString(string pattern, string instanceName, string databaseName, string userName, System.Security.SecureString password)
        {
            if (string.IsNullOrWhiteSpace(pattern))
                throw new ArgumentNullException(nameof(pattern));
            if (string.IsNullOrWhiteSpace(databaseName))
                throw new ArgumentNullException(nameof(databaseName));
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(nameof(userName));
            if (password is null)
                throw new ArgumentNullException(nameof(password));
            if (instanceName is null)
                instanceName = string.Empty;
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, pattern, BuildLocalServerInstanceName(instanceName), databaseName, userName, password);
        }

        /// <summary>
        /// Returns a server name in the format 'Machine Name' or 'Machine Name\Instance name',
        /// e.g., "ABC\SQLEXPRESS".
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="instanceName"> Specifies the local server instance name. </param>
        /// <returns> A String. </returns>
        public static string BuildLocalServerInstanceName(string instanceName)
        {
            if (instanceName is null)
            {
                instanceName = string.Empty;
            }

            string dataSourcePattern = @"{0}\{1}";
            if (string.IsNullOrWhiteSpace(instanceName))
            {
                return "LocalHost"; // Environment.MachineName
            }
            else
            {
                return string.Format(System.Globalization.CultureInfo.CurrentCulture, dataSourcePattern, Environment.MachineName, instanceName);
            }
        }

        /// <summary>
        /// Returns a connection string to an SQL catalog on the specified local server instance.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="instanceName"> Specifies the local server instance name. </param>
        /// <param name="databaseName"> Specifies the database name. </param>
        /// <returns> A String. </returns>
        public static string BuildLocalConnectionString(string instanceName, string databaseName)
        {
            if (instanceName is null)
            {
                instanceName = string.Empty;
            }

            return string.IsNullOrWhiteSpace(databaseName)
                ? throw new ArgumentNullException(nameof(databaseName))
                : BuildLocalConnectionString(ConnectionStringPattern, instanceName, databaseName);
        }

        #endregion

        #region " PARSERS "

        /// <summary>
        /// Parses the connection string getting the server, instance,and database names.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="connectionString"> The connection string. </param>
        public override void ParseConnectionString(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            // data source=MUSCAT\SQLEXPRESS;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe;
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            var syntaxParts = new Queue<string>(connectionString.Split(';'));
            if (syntaxParts.Count < 2)
                throw new ArgumentException($"Invalid connection string '{connectionString}'.", nameof(connectionString));
            this.DataSourceCompatibilityLevel = 2;
            var dataSources = new Queue<string>(syntaxParts.Dequeue().Split('='));
            if (dataSources.Count < 2)
                throw new ArgumentException($"Invalid connection string data sources '{connectionString}'.", nameof(connectionString));
            _ = dataSources.Dequeue();
            this.ServerName = dataSources.Dequeue();
            var serverElements = new Queue<string>( this.ServerName.Split('\\'));
            if (serverElements.Any())
                this.HostName = serverElements.Dequeue();
            if (serverElements.Any())
                this.InstanceName = serverElements.Dequeue();
            var elements = new Queue<string>(syntaxParts.Dequeue().Split('='));
            _ = elements.Dequeue();
            if (elements.Any())
                this.DatabaseName = elements.Dequeue();
            string userId = string.Empty;
            string candidate;
            var ss = new System.Security.SecureString();
            while (syntaxParts.Any())
            {
                candidate = syntaxParts.Dequeue();
                if (candidate.StartsWith("user id", StringComparison.CurrentCultureIgnoreCase))
                {
                    userId = candidate.Split('=').Last();
                }
                else if (candidate.StartsWith("password", StringComparison.InvariantCultureIgnoreCase))
                {
                    foreach (char cc in candidate.Split('=').Last().ToCharArray())
                        ss.AppendChar(cc);
                    ss.MakeReadOnly();
                }
            }

            if (!(string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(ss.ToString())))
            {
                this.Credentials = new System.Data.SqlClient.SqlCredential(userId, ss);
            }

            // this requires SQL Management Objects
            this.DatabaseFileInfo = null;
        }

        #endregion

        #endregion

        #region " CONNECTION "

        /// <summary>   Pings the database server. </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <param name="pingHops"> (Optional) The ping hops. </param>
        /// <returns>   <c>True</c> if the server was located; Otherwise, <c>False</c>. </returns>
        public override bool PingHost(TimeSpan timeout, int pingHops = 10 )
        {
            return FastPing( this.ServerName, pingHops, timeout);
        }

        /// <summary> Creates the connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> The new connection. </returns>
        public static System.Data.SqlClient.SqlConnection CreateConnection()
        {
            System.Data.SqlClient.SqlConnection conn = null;
            System.Data.SqlClient.SqlConnection temp = null;
            try
            {
                temp = new System.Data.SqlClient.SqlConnection();
                conn = temp;
                temp = null;
            }
            catch
            {
                throw;
            }
            finally
            {
                temp?.Dispose();
            }

            return conn;
        }

        /// <summary> Opens a connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectionString"> The connection string. </param>
        /// <returns> A SqlClient.SqlConnection. </returns>
        public static System.Data.SqlClient.SqlConnection OpenConnection(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            System.Data.SqlClient.SqlConnection conn = null;
            System.Data.SqlClient.SqlConnection temp = null;
            try
            {
                temp = CreateConnection();
                temp.ConnectionString = connectionString;
                temp.Open();
                conn = temp;
                temp = null;
            }
            catch
            {
                throw;
            }
            finally
            {
                temp?.Dispose();
            }

            return conn;
        }

        /// <summary>
        /// Returns true if able to connect to the database using the specified connection string.
        /// </summary>
        /// <remarks> David, 2020-03-16. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectionString">   Specifies the connection string to connect to. </param>
        /// <param name="keepConnectionOpen"> True to keep connection open. </param>
        /// <returns> <c>True</c> if success; Otherwise, <c>False</c>. </returns>
        public override (bool Success, string Details) TryConnect(string connectionString, bool keepConnectionOpen)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            string activity = $"opening {connectionString}";
            System.Data.SqlClient.SqlConnection conn = null;
            (bool success, string details) outcome = (true, string.Empty);
            try
            {
                conn = OpenConnection(connectionString);
                this.LastConnectionFailed = IsClosed(conn);
                if ( this.LastConnectionFailed )
                {
                    outcome = (!this.LastConnectionFailed, "Connection failed to open");
                }
            }
            catch (Exception ex)
            {
                conn?.Dispose();
                conn = null;
                this.LastConnectionFailed = true;
                _ = ex.AddExceptionData();
                outcome = (!this.LastConnectionFailed, $"Exception {activity};. {ex}");
            }
            finally
            {
                if (!keepConnectionOpen)
                {
                    // apparently, close also disposes. otherwise, uncomment this:
                    // If not DatabaseManager.IsClosed(connection) Then conn.Close()
                    conn?.Dispose();
                }
            }

            return outcome;
        }

        #endregion

        #region " COMMAND "

        /// <summary> Executes the specified command and returns the number of rows affected. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="commandText">      Specifies a command. </param>
        /// <returns> The number of rows affected. </returns>
        public override int ExecuteCommand(string connectionString, string commandText)
        {
            int rowCount = 0;
            using (var connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                var sqlCommand = connection.CreateCommand();
                sqlCommand.CommandText = commandText;
                connection.Open();
                rowCount = sqlCommand.ExecuteNonQuery();
                // apparently close also disposes. Otherwise, include: connection.Close()
            }

            return rowCount;
        }

        #endregion

        #region " DATABASE "

        /// <summary> Connects to the specified database on the specified local server instance. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="instanceName"> Specifies the server instance name, such as 'SQLEXPRESS'. </param>
        /// <param name="databaseName"> Name of the database. </param>
        /// <returns> A SqlClient.SqlConnection. </returns>
        public static System.Data.SqlClient.SqlConnection ConnectLocalDatabase(string instanceName, string databaseName)
        {
            if (instanceName is null)
            {
                instanceName = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(databaseName))
            {
                throw new ArgumentNullException(nameof(databaseName));
            }

            // Get the connection string to the master catalog
            string connString = BuildLocalConnectionString(instanceName, databaseName);
            System.Data.SqlClient.SqlConnection conn = new ( connString );
            if (IsClosed(conn))
            {
                conn.Open();
            }
            return conn;
        }

        #endregion

        #region " READER "

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
        public override ConnectedReader ExecuteReader(string connectionString, string queryCommand)
        {
            var c = new System.Data.SqlClient.SqlConnection(connectionString);
            return ExecuteReader(c, queryCommand);
        }

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">   Specifies a
        /// <see cref="System.Data.SqlClient.SqlConnection">connection</see>. </param>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> A ConnectedReader. </returns>
        public static ConnectedReader ExecuteReader(System.Data.SqlClient.SqlConnection connection, string queryCommand)
        {
            if (connection is null)
                throw new ArgumentNullException(nameof(connection));
            var command = new System.Data.SqlClient.SqlCommand()
            {
                Connection = connection,
                CommandTimeout = 15,
                CommandType = CommandType.Text,
                CommandText = queryCommand
            };
            if (IsClosed(connection))
            {
                command.Connection.Open();
            }

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            return new ConnectedReader(connection, reader);
        }

        #endregion

        #region " UPDATE QUERY "

        /// <summary> Executes a table update query. </summary>
        /// <remarks>
        /// If using multiple SQL commands, each command ends with a ';' followed by a new line.
        /// </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public override int UpdateTableQuery(string connectionString, string query, bool ignoreMinorErrors)
        {
            return this.UpdateTableQuery(connectionString, PrepareCommandQuery(query), ignoreMinorErrors);
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks>
        /// If using multiple SQL commands, each command ends with a ';' followed by a new line.
        /// </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public override int UpdateTableQuery(string connectionString, string[] query, bool ignoreMinorErrors)
        {
            using var connection = new System.Data.SqlClient.SqlConnection( connectionString );
            return this.UpdateTableQuery( connection, query, ignoreMinorErrors );
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks>
        /// If using multiple SQL commands, each command ends with a ';' followed by a new line.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">        Specifies the database connection. </param>
        /// <param name="query">             Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public int UpdateTableQuery(System.Data.SqlClient.SqlConnection connection, string[] query, bool ignoreMinorErrors)
        {
            if (connection is null)
                throw new ArgumentNullException(nameof(connection));
            var recordCount = default(int);
            var isOpened = default(bool);
            this.QueryUpdateCount = 0;
            this.QueryErrorCount = 0;
            if (query is object)
            {
                using (var command = new System.Data.SqlClient.SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    if (IsClosed(connection))
                    {
                        isOpened = true;
                        connection.Open();
                    }

                    foreach (string queryCommand in query)
                    {
                        if (!string.IsNullOrWhiteSpace(queryCommand))
                        {
                            command.CommandText = queryCommand;
                            if (ignoreMinorErrors)
                            {
                                try
                                {
                                    // ? each update returns -1 if success or and error  if failed.
                                    recordCount += command.ExecuteNonQuery();
                                    this.QueryUpdateCount += 1;
                                }
                                catch
                                {
                                    this.QueryErrorCount += 1;
                                }
                                finally
                                {
                                }
                            }
                            else
                            {
                                recordCount += command.ExecuteNonQuery();
                                this.QueryUpdateCount += 1;
                            }
                        }
                    }
                }

                if (isOpened)
                {
                    connection.Close();
                }
            }

            return recordCount;
        }

        #endregion

    }
}
