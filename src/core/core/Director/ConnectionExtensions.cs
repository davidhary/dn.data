﻿using System;
using System.Data;

namespace isr.Data.Core.ConnectionExtensions
{

    /// <summary> Includes extensions for <see cref="IDbConnection">Connection</see>. </summary>
        /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        #region " CONNECTION STATE "

        /// <summary> Returns true if 'connection' is closed. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if closed; otherwise <c>false</c> </returns>
        public static bool IsClosed(this IDbConnection connection)
        {
            return connection is null ? throw new ArgumentNullException(nameof(connection)) : connection.State == ConnectionState.Closed;
        }

        /// <summary> Returns true if 'connection' is open. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if open; otherwise <c>false</c> </returns>
        public static bool IsOpen(this IDbConnection connection)
        {
            return connection is null
                ? throw new ArgumentNullException(nameof(connection))
                : (connection.State & ConnectionState.Open) == ConnectionState.Open;
        }

        #endregion

    }
}