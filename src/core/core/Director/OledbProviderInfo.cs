using System;
using System.Collections.Generic;
using System.Data;

#if NET6_0_OR_GREATER
#if WINDOWS

namespace isr.Data.Core
{

    /// <summary> An OLE database provider. </summary>
    /// <remarks>
    /// (c) 2018 PIEBALD consulting. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-23. https://www.codeproject.com/Tips/1231609/Enumerating-OleDb-Providers.
    /// </para>
    /// </remarks>
    public sealed class OledbProviderInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks>
        /// See: <para>
        /// https://msdn.microsoft.com/en-us/library/system.data.oledb.oledbenumerator.getrootenumerator(v=vs.110).aspx
        /// </para><para>
        /// 0 SOURCES_NAME </para><para>
        /// 1 SOURCES_PARSENAME </para><para>
        /// 2 SOURCES_DESCRIPTION </para><para>
        /// 3 SOURCES_TYPE  One of the following enumeration members: </para><para>
        /// Binder (0) </para><para>
        /// DataSource_MDP (1) </para><para>
        /// DataSource_TDP (2) </para><para>
        /// Enumerator (3). </para><para>
        /// These correspond to the values returned in the SOURCES_TYPE column  </para><para>
        /// of the native OLE DB sources row set. </para><para>
        /// 4 SOURCES_ISPARENT </para><para>
        /// 5 SOURCES_CLSID </para>
        /// </remarks>
        /// <param name="record"> The record. </param>
        public OledbProviderInfo(IDataRecord record) : base()
        {
            if (record is object)
            {
                this.Name = Convert.ToString(record["SOURCES_NAME"]);
                this.ParseName = Guid.Parse(Convert.ToString(record["SOURCES_PARSENAME"]));
                this.ProviderType = (ProviderType)Convert.ToInt32(record["SOURCES_TYPE"]);
                this.Description = Convert.ToString(record["SOURCES_DESCRIPTION"]);
                this.IsParent = Convert.ToBoolean(record["SOURCES_ISPARENT"]);
                this.ClassId = Guid.Parse(Convert.ToString(record["SOURCES_CLSID"]));
            }
        }

        /// <summary> Build unique identifier. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="oledbProviderInfo"> Information describing the oledb provider. </param>
        /// <returns> A String. </returns>
        public static string BuildUniqueId(OledbProviderInfo oledbProviderInfo)
        {
            return oledbProviderInfo is null
                ? throw new ArgumentNullException(nameof(oledbProviderInfo))
                : $"{oledbProviderInfo.Name}.{(int)oledbProviderInfo.ProviderType}";
        }

        /// <summary> Gets a unique identifier. </summary>
        /// <value> The identifier of the unique. </value>
        public string UniqueId => BuildUniqueId( this );

        /// <summary> Gets or sets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; private set; }

        /// <summary> Gets or sets the name of the parse. </summary>
        /// <value> The name of the parse. </value>
        public Guid ParseName { get; private set; }

        /// <summary> Gets or sets the description. </summary>
        /// <value> The description. </value>
        public string Description { get; private set; }

        /// <summary> Gets or sets the type of the provider. </summary>
        /// <value> The type of the provider. </value>
        public ProviderType ProviderType { get; private set; }

        /// <summary> Gets or sets the is parent. </summary>
        /// <value> The is parent. </value>
        public bool IsParent { get; private set; }

        /// <summary> Gets or sets the identifier of the class. </summary>
        /// <value> The identifier of the class. </value>
        public Guid ClassId { get; private set; }
    }

        /// <summary> Collection of oledb provider informations. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-24 </para>
    /// </remarks>
    public class OledbProviderInfoCollection : System.Collections.ObjectModel.KeyedCollection<string, OledbProviderInfo>
    {

        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override string GetKeyForItem(OledbProviderInfo item)
        {
            return item is null ? throw new ArgumentNullException(nameof(item)) : item.UniqueId;
        }

        /// <summary> Gets the providers. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns> An OledbProviderInfoCollection. </returns>
        public static OledbProviderInfoCollection Providers()
        {
            var result = new OledbProviderInfoCollection();
            _ = result.EnumerateProviders();
            return result;
        }


        /// <summary> Enumerate providers. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the providers in this collection.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Interoperability", "CA1416:Validate platform compatibility", Justification = "<Pending>" )]
        public IEnumerable<OledbProviderInfo> EnumerateProviders()
        {
            this.Clear();
            var l = new List<OledbProviderInfo>();
            using ( IDataReader dr = System.Data.OleDb.OleDbEnumerator.GetRootEnumerator() )
            {
                while (dr.Read())
                {
                    var value = new OledbProviderInfo(dr);
                    l.Add(value);
                    // apparently, name + type is not unique. Neither is the class id.
                    if (!this.Contains(value.UniqueId))
                        this.Add(value);
                }
            }

            return l;
        }
    }

        /// <summary> Values that represent provider types. </summary>
    /// <remarks> David, 2020-10-08. </remarks>
    public enum ProviderType
    {

        /// <summary> An enum constant representing the binder option. </summary>
        Binder = 0,

        /// <summary> An enum constant representing the data source Mdp option. </summary>
        DataSourceMdp = 1,

        /// <summary> An enum constant representing the data source Tdp option. </summary>
        DataSourceTdp = 2,

        /// <summary> Values that represent enumerators. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        Enumerator = 3
    }

}

#endif
#endif
