using System;
using System.ComponentModel;

namespace isr.Data.Core
{

    /// <summary> Parses connection strings. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-10-21, 1.2.3581.x. </para>
    /// </remarks>
    public abstract class ConnectionInfoBase : INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ConnectionInfoBase" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        protected ConnectionInfoBase(string connectionString) : base()
        {
            this._ConnectionString = connectionString;
        }

        /// <summary> Initializes a new instance of the <see cref="ConnectionInfoBase" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <param name="databaseName">     Name of the database. </param>
        protected ConnectionInfoBase(string connectionString, string databaseName) : this(connectionString)
        {
            this._DatabaseName = databaseName;
        }

        #endregion

        #region " I NOTIFY PROPERTY CHANGED IMPLEMENTATION "

        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>   Notifies a property changed. </summary>
        /// <remarks>   David, 2021-02-01. </remarks>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] String propertyName = "" )
        {
            this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        /// <summary>   Removes the property changed event handlers. </summary>
        /// <remarks>   David, 2021-06-28. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            var handler = this.PropertyChanged;
            if ( handler is object )
            {
                foreach ( var item in handler.GetInvocationList() )
                {
                    handler -= ( PropertyChangedEventHandler ) item;
                }
            }
        }

        #endregion

        #region " CONNECTION INFO "

        /// <summary> The data source compatibility level. </summary>
        private int _DataSourceCompatibilityLevel;

        /// <summary> Gets or sets the SQL compatibility level for this entity. </summary>
        /// <value> The data source compatibility level. </value>
        public int DataSourceCompatibilityLevel
        {
            get => this._DataSourceCompatibilityLevel;

            set {
                if ( !this.DataSourceCompatibilityLevel.Equals( value ) )
                {
                    this._DataSourceCompatibilityLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The connection string. </summary>
        private string _ConnectionString;

        /// <summary> Gets or sets the connection string. </summary>
        /// <value> The connection string. </value>
        public string ConnectionString
        {
            get => this._ConnectionString;

            set {
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    this.ParseConnectionString( value );
                }

                this._ConnectionString = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Information describing the database file. </summary>
        private System.IO.FileInfo _DatabaseFileInfo;

        /// <summary> Gets or sets (protected) the database file information. </summary>
        /// <value> Information describing the database file. </value>
        public System.IO.FileInfo DatabaseFileInfo
        {
            get => this._DatabaseFileInfo;

            protected set {
                if ( value is null && this.DatabaseFileInfo is object )
                {
                    this._DatabaseFileInfo = value;
                    this.NotifyPropertyChanged();
                }
                else if ( value is object && !value.Equals( this.DatabaseFileInfo ) )
                {
                    this._DatabaseFileInfo = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the database. </summary>
        private string _DatabaseName;

        /// <summary> Gets or sets the database name. </summary>
        /// <value> The name of the database. </value>
        public string DatabaseName
        {
            get => this._DatabaseName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.DatabaseName ) )
                {
                    this._DatabaseName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the host. </summary>
        private string _HostName;

        /// <summary> Gets or sets the database server Host name. </summary>
        /// <value> The name of the Host. </value>
        public string HostName
        {
            get => this._HostName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.HostName ) )
                {
                    this._HostName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the instance. </summary>
        private string _InstanceName; // 

        /// <summary> Gets or sets the database server instance name. </summary>
        /// <value> The name of the instance. </value>
        public string InstanceName
        {
            get => this._InstanceName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.InstanceName ) )
                {
                    this._InstanceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the server. </summary>
        private string _ServerName;

        /// <summary> Gets or sets the database server name. </summary>
        /// <value> The name of the server. </value>
        public string ServerName
        {
            get => this._ServerName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.ServerName ) )
                {
                    this._ServerName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The credentials. </summary>
        private System.Data.SqlClient.SqlCredential _Credentials;

        /// <summary> Gets or sets the credentials. </summary>
        /// <value> The credentials. </value>
        public System.Data.SqlClient.SqlCredential Credentials
        {
            get => this._Credentials;

            set {
                if ( value is object && !(string.Equals( value?.UserId, this.Credentials?.UserId ) && Equals( value?.Password, this.Credentials?.Password )) )
                {
                    this._Credentials = new System.Data.SqlClient.SqlCredential( value.UserId, value.Password );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " PARSERS "

        /// <summary>
        /// Parses the connection string getting the server, instance,and database names.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        public abstract void ParseConnectionString(string connectionString);

        #endregion

    }
}
