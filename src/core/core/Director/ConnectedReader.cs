using System;
using System.Data;
using System.Diagnostics;

namespace isr.Data.Core
{

    /// <summary> Implements a database connection with an open reader. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-10-21, 1.2.3581.x. </para>
    /// </remarks>
    public class ConnectedReader : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ConnectedReader" /> class. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="reader">     The reader. </param>
        public ConnectedReader(IDbConnection connection, IDataReader reader) : base()
        {
            this.Connection = connection;
            this.Reader = reader;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose(true);

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.CloseConnection();
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        #endregion

        /// <summary> Gets or sets the connection. </summary>
        /// <value> The connection. </value>
        public IDbConnection Connection { get; private set; }

        /// <summary> Gets or sets the reader. </summary>
        /// <value> The reader. </value>
        public IDataReader Reader { get; private set; }

        /// <summary> Closes the reader and the connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public void CloseConnection()
        {
            if ( this.Reader is object)
            {
                this.Reader.Close();
                this.Reader.Dispose();
            }

            if ( this.Connection is object)
            {
                this.Connection.Close();
                this.Connection.Dispose();
            }
        }
    }
}
