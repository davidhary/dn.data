using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

using isr.Data.Core.ExceptionExtensions;

namespace isr.Data.Core
{

    /// <summary> Base data manager class. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-10-21, 1.2.3581.x. </para>
    /// </remarks>
    public abstract class DatabaseManagerBase : ConnectionInfoBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        protected DatabaseManagerBase( string connectionString ) : base( connectionString )
        {
            this._CurrentReleaseVersion = string.Empty;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManagerBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> The connection string. </param>
        /// <param name="databaseName">     Name of the database. </param>
        protected DatabaseManagerBase( string connectionString, string databaseName ) : base( connectionString, databaseName )
        {
            this._CurrentReleaseVersion = string.Empty;
        }

        #endregion

        #region " PRESET "

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        public abstract void InitializeKnownState();

        #endregion

        #region " CONNECTION MANAGER "

        /// <summary> True if last connection failed. </summary>
        private bool _LastConnectionFailed;

        /// <summary> Gets or sets the outcome of last connection attempt. </summary>
        /// <value> The last connection failed. </value>
        public bool LastConnectionFailed
        {
            get => this._LastConnectionFailed;

            set {
                if ( !this.LastConnectionFailed.Equals( value ) )
                {
                    this._LastConnectionFailed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Pings the database host. With file-based databases, such as SQL Compact, this tests if the
        /// database file exists.
        /// </summary>
        /// <remarks>   David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="timeout">  The timeout. </param>
        /// <param name="pingHops"> (Optional) The ping hops. </param>
        /// <returns>   <c>True</c> if the server was located; Otherwise, <c>False</c>. </returns>
        public abstract bool PingHost( TimeSpan timeout, int pingHops = 3 );

        /// <summary> tries to open a connection. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> A Tuple(Of TraceEventType, String) </returns>
        public static (TraceEventType Outcome, string Details) TryOpen( IDbConnection connection )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            string activity = string.Empty;
            var item1 = TraceEventType.Verbose;
            string item2 = string.Empty;
            try
            {
                if ( string.IsNullOrWhiteSpace( connection.ConnectionString ) )
                {
                    item1 = TraceEventType.Warning;
                    item2 = "Connection string not specified";
                }
                else
                {
                    bool wasOpen = ConnectionState.Open == (connection.State & ConnectionState.Open);
                    if ( wasOpen )
                    {
                        item2 = "Connection was open";
                    }
                    else
                    {
                        activity = $"opening {connection.ConnectionString}";
                        connection.Open();
                        if ( connection.State == ConnectionState.Closed )
                        {
                            item1 = TraceEventType.Warning;
                            item2 = $"Connection {connection.ConnectionString} failed to open";
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                item1 = TraceEventType.Error;
                _ = ex.AddExceptionData();
                item2 = $"Exception {activity};. {ex}";
            }
            finally
            {
            }

            return ( item1, item2 );
        }

        /// <summary>
        /// Returns true if able to connect to the database using the specified connection string.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="keepConnectionOpen"> True to keep connection open. </param>
        /// <returns>  <see cref="Tuple{T1, T2}"/> <c>True</c> if success; Otherwise, <c>False</c>; Details if failure. </returns>
        public (bool Success, string Details) TryConnect( bool keepConnectionOpen )
        {
            return this.TryConnect( this.ConnectionString, keepConnectionOpen );
        }

        /// <summary>
        /// Returns true if able to connect to the database using the specified connection string.
        /// </summary>
        /// <remarks> David, 2020-03-16. </remarks>
        /// <param name="connectionString">   Specifies the connection string to connect to. </param>
        /// <param name="keepConnectionOpen"> True to keep connection open. </param>
        /// <returns>  <see cref="Tuple{T1, T2}"/> <c>True</c> if success; Otherwise, <c>False</c>; Details if failure. </returns>
        public abstract (bool Success, string Details) TryConnect( string connectionString, bool keepConnectionOpen );

        /// <summary> Query if 'connection' is closed. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> <c>true</c> if closed; otherwise <c>false</c> </returns>
        public static bool IsClosed( IDbConnection connection )
        {
            return connection is null ? throw new ArgumentNullException( nameof( connection ) ) : connection.State == ConnectionState.Closed;
        }

        /// <summary> Query if 'connection' is open. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection"> The connection. </param>
        /// <returns> True if open, false if not. </returns>
        public static bool IsOpen( IDbConnection connection )
        {
            return connection is null
                ? throw new ArgumentNullException( nameof( connection ) )
                : (connection.State & ConnectionState.Open) == ConnectionState.Open;
        }

        #endregion

        #region " COMMAND "

        /// <summary> Executes the command operation. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the database connection string. </param>
        /// <param name="commandText">      The command text. </param>
        /// <returns> The number of rows affected. </returns>
        public abstract int ExecuteCommand( string connectionString, string commandText );

        #endregion

        #region " QUERY "

        /// <summary> Executes the specified query command and returns a 64-bit signed value. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <returns> The 64-bit signed value of the field. </returns>
        public long QueryInt64( string connectionString, string queryCommand )
        {
            return this.ExecuteReader( connectionString, queryCommand ).Reader.GetInt64( 0 );
        }

        /// <summary> Executes the specified query command and returns a long integer. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> The 64-bit signed value of the field. </returns>
        public long QueryInt64( string queryCommand )
        {
            return this.QueryInt64( this.ConnectionString, queryCommand );
        }

        /// <summary> Reads a string type field from the database using the specified query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <param name="columnIndex">      Specifies the index where the returned value resides. </param>
        /// <returns> The string value of the specified field. </returns>
        public string QueryString( string connectionString, string queryCommand, int columnIndex )
        {
            using var connReader = this.ExecuteReader( connectionString, queryCommand );
            return connReader.Reader.Read() ? connReader.Reader.GetString( columnIndex ) : string.Empty;
        }

        /// <summary> Reads a string type field from the database using the specified query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <param name="columnName">       Specifies the column name where the returned value resides. </param>
        /// <returns> The string value of the specified field. </returns>
        public string QueryString( string connectionString, string queryCommand, string columnName )
        {
            using var connReader = this.ExecuteReader( connectionString, queryCommand );
            if ( connReader.Reader.Read() )
            {
                int columnIndex = connReader.Reader.GetOrdinal( columnName );
                return columnIndex >= 0 ? connReader.Reader.GetString( columnIndex ) : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region " PROVIDERS "

#if NET6_0_OR_GREATER
        /// <summary> Gets the installed providers. </summary>
        /// <value> The installed providers. </value>
        public static DataTable InstalledProviders => System.Data.Common.DbProviderFactories.GetFactoryClasses();
#endif

        /// <summary> Convert this object into a string representation. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="table"> The table. </param>
        /// <returns> A String that represents this object. </returns>
        public static string ToString( DataTable table )
        {
            if ( table is null )
                throw new ArgumentNullException( nameof( table ) );
            var builder = new System.Text.StringBuilder();
            foreach ( DataColumn column in table.Columns )
                _ = builder.Append( $"{(builder.Length > 0 ? "," : "")}{column.ColumnName}" );
            foreach ( DataRow row in table.Rows )
            {
                var colBuilder = new System.Text.StringBuilder();
                foreach ( DataColumn column in table.Columns )
                    _ = colBuilder.Append( $"{(colBuilder.Length > 0 ? "," : "")}{row[column]}" );
                _ = builder.AppendLine( colBuilder.ToString() );
            }

            return builder.ToString();
        }

#endregion

#region " READER "

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
        public abstract ConnectedReader ExecuteReader( string connectionString, string queryCommand );

        /// <summary>
        /// Executes the specified query command and returns reference to the
        /// <see cref="ConnectedReader">connected reader</see>
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="queryCommand"> Specifies a query. </param>
        /// <returns> An instance of the <see cref="ConnectedReader">reader</see>. </returns>
        public ConnectedReader ExecuteReader( string queryCommand )
        {
            return this.ExecuteReader( this.ConnectionString, queryCommand );
        }

#endregion

#region " TABLES "

        /// <summary> Returns true if a record exists satisfying the query command. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="queryCommand">     Specifies a query. </param>
        /// <returns>
        /// <c>True</c> if a record exists satisfying the query command; Otherwise, <c>False</c>.
        /// </returns>
        public bool RecordExists( string connectionString, string queryCommand )
        {
            using var connReader = this.ExecuteReader( connectionString, queryCommand );
            return connReader.Reader.Read();
        }

        /// <summary> Returns true if the specified table exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <param name="value">            Specifies the table name. </param>
        /// <returns> <c>True</c> if a table exists; Otherwise, <c>False</c>. </returns>
        public bool TableExists( string connectionString, string value )
        {
            string queryCommand = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{0}'";
            queryCommand = string.Format( System.Globalization.CultureInfo.CurrentCulture, queryCommand, value );
            return this.RecordExists( connectionString, queryCommand );
        }

        /// <summary> Returns true if the specified table exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="value"> Specifies the table name. </param>
        /// <returns> <c>True</c> if a table exists; Otherwise, <c>False</c>. </returns>
        public bool TableExists( string value )
        {
            return this.TableExists( this.ConnectionString, value );
        }

        /// <summary> Gets the number of distinct SQL commands defined in the specified query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="query"> Specifies the query or file name where the query is located. A file name
        /// begins with '@'. </param>
        /// <returns> An Integer. </returns>
        public static int DeriveCommandCount( string query )
        {
            return PrepareCommandQuery( query ).Length;
        }

        /// <summary>
        /// Prepares a query for processing as a connection command. If preceded with '@' the query is
        /// retrieved form file. Multiple commands must be delimited with a ';' and CR or ';' and CR-LF.
        /// </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="query"> Specifies a query string or a file name. </param>
        /// <returns> A String(). </returns>
        public static string[] PrepareCommandQuery( string query )
        {
            if ( string.IsNullOrWhiteSpace( query ) || query.Length < 2 )
            {
                return new string[] { "" };
            }
            else
            {

                // check if using file or query
                if ( query.Substring( 0, 1 ) == "@" )
                {
                    // if the is a file name then get the file name
                    string filePath = query.Substring( 1 );
                    query = System.IO.File.ReadAllText( filePath );
                }

                // split queries to lines
                string[] lines;
                lines = query.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
                var commandList = new List<string>();
                var commandLine = new System.Text.StringBuilder();
                foreach ( string line in lines )
                {
                    if ( commandLine.Length > 0 )
                    {
                        _ = commandLine.AppendLine();
                    }

                    _ = commandLine.Append( line );
                    if ( line.Trim().EndsWith( ";", StringComparison.OrdinalIgnoreCase ) || line.Trim().EndsWith( "GO", StringComparison.OrdinalIgnoreCase ) )
                    {
                        commandList.Add( commandLine.ToString() );
                        commandLine = new System.Text.StringBuilder();
                    }
                }

                return commandList.ToArray();
            }
        }

        /// <summary> Number of query errors. </summary>
        private int _QueryErrorCount;

        /// <summary> Gets or sets (protected) the number of update queries that failed. </summary>
        /// <value> The number of query errors. </value>
        public int QueryErrorCount
        {
            get => this._QueryErrorCount;

            protected set {
                this._QueryErrorCount = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Number of query updates. </summary>
        private int _QueryUpdateCount;

        /// <summary> Gets or sets (protected) the number of update query that worked. </summary>
        /// <value> The number of query updates. </value>
        public int QueryUpdateCount
        {
            get => this._QueryUpdateCount;

            protected set {
                this._QueryUpdateCount = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query or file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public abstract int UpdateTableQuery( string connectionString, string query, bool ignoreMinorErrors );

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString">  Specifies the database connection string. </param>
        /// <param name="query">             Specifies the query lines. </param>
        /// <param name="ignoreMinorErrors"> Allows ignoring minor SQL error on a complex multi-line
        /// command. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public abstract int UpdateTableQuery( string connectionString, string[] query, bool ignoreMinorErrors );

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="query"> Specifies the query or file name where the query is located. A file name
        /// begins with '@'. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public int UpdateTableQuery( string query )
        {
            return this.UpdateTableQuery( this.ConnectionString, query );
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="query"> Specifies the query lines. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public int UpdateTableQuery( string[] query )
        {
            return this.UpdateTableQuery( this.ConnectionString, query );
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the database connection string. </param>
        /// <param name="query">            Specifies the query of file name where the query is located.
        /// A file name begins with '@'. </param>
        /// <returns>
        /// The total number of minor errors minus the number of successful command executions.
        /// </returns>
        public int UpdateTableQuery( string connectionString, string query )
        {
            var recordCount = default( int );
            if ( !string.IsNullOrWhiteSpace( query ) )
            {
                recordCount = this.UpdateTableQuery( connectionString, query, false );
            }

            return recordCount;
        }

        /// <summary> Executes a table update query. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the database connection string. </param>
        /// <param name="query">            Specifies the query lines. </param>
        /// <returns> The number of records affected by the query. </returns>
        public int UpdateTableQuery( string connectionString, string[] query )
        {
            var recordCount = default( int );
            if ( query is object )
            {
                recordCount = this.UpdateTableQuery( connectionString, query, false );
            }

            return recordCount;
        }

#endregion

#region " VERIFICATION "

        /// <summary> Gets or sets the sentinel indicating a failed release. </summary>
        /// <value> The failed release. </value>
        protected string FailedRelease { get; private set; } = "<failed>";

        /// <summary> The current release version. </summary>
        private string _CurrentReleaseVersion;

        /// <summary>
        /// Gets or sets (protected) the current version. Reads from the database if the stored version
        /// is null or fails reading the version or last verification failed.
        /// </summary>
        /// <value> The current release version. </value>
        public string CurrentReleaseVersion
        {
            get => this._CurrentReleaseVersion;

            protected set {
                this._CurrentReleaseVersion = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The current release version query text. </summary>
        private string _CurrentReleaseVersionQueryText;

        /// <summary> Gets or sets (protected) the text used to query the current release. </summary>
        /// <value> The current release version query text. </value>
        public string CurrentReleaseVersionQueryText
        {
            get => this._CurrentReleaseVersionQueryText;

            set {
                this._CurrentReleaseVersionQueryText = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True if database verified. </summary>
        private bool _DatabaseVerified;

        /// <summary>
        /// Gets or sets (protected) the sentinel indicating if the database was successfully verified.
        /// </summary>
        /// <value> The database verified. </value>
        public bool DatabaseVerified
        {
            get => this._DatabaseVerified;

            protected set {
                this._DatabaseVerified = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Queries the current version. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <param name="connectionString"> Specifies the connection string. </param>
        /// <returns> The current release version. </returns>
        public string QueryCurrentReleaseVersion( string connectionString )
        {
            this.CurrentReleaseVersion = this.QueryString( connectionString, this.CurrentReleaseVersionQueryText, 0 );
            return this.CurrentReleaseVersion;
        }

        /// <summary> Verifies that the local database is connectible and is current. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connectionString"> Specifies the database connection string. </param>
        /// <param name="version">          Specifies the database version. </param>
        /// <returns>  <see cref="Tuple{T1, T2}"/> <c>True</c> if success; Otherwise, <c>False</c>; Details if failure. </returns>
        public (bool Success, string Details) VerifyDatabaseVersion( string connectionString, string version )
        {
            // clear the verification status.
            this.DatabaseVerified = false;

            // set default properties.
            (bool success, string details) = this.TryConnect( connectionString, true );
            if ( success )
            {
                _ = this.QueryCurrentReleaseVersion( connectionString );
                this.DatabaseVerified = Equals( new Version( this.CurrentReleaseVersion ), new Version( version ) );
                if ( !this.DatabaseVerified )
                {
                    return (false, $"Local database version mismatch;. Expected version '{version}' and found '{this.CurrentReleaseVersion}'");
                }
            }

            return (success, details);
        }

#endregion

#region " SHARED COMMAND "

        /// <summary> Executes the command operation. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataConnection"> The data connection. </param>
        /// <param name="commandText">    The command text. </param>
        /// <returns> An Integer. </returns>
        public static int ExecuteCommand( IDbConnection dataConnection, string commandText )
        {
            if ( dataConnection is null )
                throw new ArgumentNullException( nameof( dataConnection ) );
            if ( string.IsNullOrEmpty( commandText ) )
                return 0;
            var command = dataConnection.CreateCommand();
            command.CommandText = commandText;
            if ( dataConnection.State != ConnectionState.Open )
                dataConnection.Open();
            int rowCount = command.ExecuteNonQuery();
            return rowCount;
        }

        /// <summary> Executes the scalar operation. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dataConnection"> The data connection. </param>
        /// <param name="commandText">    The command text. </param>
        /// <returns> An Object. </returns>
        public static object ExecuteScalar( IDbConnection dataConnection, string commandText )
        {
            if ( dataConnection is null )
                throw new ArgumentNullException( nameof( dataConnection ) );
            if ( string.IsNullOrEmpty( commandText ) )
                return 0;
            var command = dataConnection.CreateCommand();
            command.CommandText = commandText;
            if ( dataConnection.State != ConnectionState.Open )
                dataConnection.Open();
            return command.ExecuteScalar();
        }

#endregion

#region " SQL TIME RANGE "

        /// <summary> Gets the SQL data time range. </summary>
        /// <value> The SQL data time range. </value>
        public static (DateTime MinDate, DateTime MaxDate) SqlDateTimeRange => (System.Data.SqlTypes.SqlDateTime.MinValue.Value, System.Data.SqlTypes.SqlDateTime.MaxValue.Value);

#endregion

#region " PING "

        /// <summary>   Fast ping. </summary>
        /// <remarks>   David, 2020-08-07. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="address">  The address. </param>
        /// <param name="pingHops"> The ping hops. </param>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool FastPing( string address, int pingHops, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( address ) )
                throw new ArgumentNullException( nameof( address ) );
            var ping = new System.Net.NetworkInformation.Ping();
            var pingOptions = new System.Net.NetworkInformation.PingOptions( pingHops, true );
            var buffer = new byte[] { 0, 0 };
            return ping.Send( address, ( int ) timeout.TotalMilliseconds, buffer, pingOptions ).Status == System.Net.NetworkInformation.IPStatus.Success;
        }

#endregion

    }
}
