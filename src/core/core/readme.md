# About

isr.Data.Core is a .Net library supporting SQL and OleDB data operations.

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Data.Core is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Data Repository].

[Data Repository]: https://bitbucket.org/davidhary/dn.data

