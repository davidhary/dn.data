
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.Core.MSTest
{

    /// <summary>   (Unit Test Class) a connection string tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class ConnectionStringTests
    {

        /// <summary>   Builds the credentials. </summary>
        /// <remarks>   David, 2022-02-07. </remarks>
        /// <param name="userName"> Name of the user. </param>
        /// <param name="password"> The password. </param>
        /// <returns>   A System.Data.SqlClient.SqlCredential. </returns>
        private System.Data.SqlClient.SqlCredential BuildCredentials( string userName, string password)   
        {
            var pw = new System.Security.SecureString();
            foreach ( char c in password.ToCharArray() )
                pw.AppendChar( c );
            pw.MakeReadOnly();
            return new System.Data.SqlClient.SqlCredential( userName, pw );
        }


        /// <summary> (Unit Test Method) tests parsing a connection string. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void ConnectionStringShouldParse()
        {
            string connectionString = @"data source=localhost\SQLX2019;initial catalog=Provers70;persist security info=False;User id=bank;Password=syncWithMe";
            var manager = new DatabaseManager( connectionString );
            manager.ParseConnectionString( connectionString );
            string expectedLocalServer = @"localhost\SQLX2019";
            Assert.AreEqual( expectedLocalServer, manager.ServerName, $"{nameof(DatabaseManager.ServerName)}");
            string expectedDatabaseName = "Provers70";
            Assert.AreEqual( expectedDatabaseName, manager.DatabaseName, $"{nameof(DatabaseManager.DatabaseName)}");
            string expectedUserName = "bank";
            string expectedPassword = "synchWithMe";
            var credentials  = this.BuildCredentials(expectedUserName, expectedPassword);
            Assert.IsNotNull(manager.Credentials, $"{typeof(DatabaseManager)}.{nameof(DatabaseManager.Credentials)} must not be null");
            Assert.AreEqual( credentials.UserId, manager.Credentials.UserId, $"{nameof(System.Data.SqlClient.SqlCredential.UserId)}");
            Assert.AreEqual( credentials.Password.ToString(), manager.Credentials.Password.ToString(), $"{nameof(System.Data.SqlClient.SqlCredential.Password)}");
        }

    }
}
