using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Data.Core.MSTest
{

    /// <summary> OLD DB Provider unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class OledbProviderTests
    {

        #region " OLE INFO "

        /// <summary> (Unit Test Method) tests the an OLEDB provider exists. </summary>
        /// <remarks> David, 2020-10-08. </remarks>
        [TestMethod()]
        public void OledbProviderShouldExists()
        {
            string excpetedOledbProviderUniqueId = "SQLOLEDB.1";
            var providers = OledbProviderInfoCollection.Providers();
            Assert.IsTrue(providers.Contains( excpetedOledbProviderUniqueId ) );
        }

        #endregion

    }
}
